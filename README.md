# Composite Pattern

The repository contains a Java application for Composite Pattern example.

## Built With

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java SE Development Kit 
* [IntelliJIDEA](https://www.jetbrains.com/idea/) - IDE for Java
* [Git](https://git-scm.com/) - The distributed version control system

## Author

* **María Ruth Vargas**